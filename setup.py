#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


setup(
    name             = "vispa_pxl_designer",
    version          = "0.1.0",
    description      = "VISPA PXL Designer - Create and execute PXL analyses.",
    author           = "VISPA Project",
    author_email     = "vispa@lists.rwth-aachen.de",
    url              = "http://vispa.physik.rwth-aachen.de/",
    license          = "GNU GPL v2",
    packages         = find_packages(),
    package_dir      = {"vispa_pxldesigner": "vispa_pxldesigner"},
    package_data     = {"vispa_pxldesigner": [
        "api/*",
        "static/css/*",
        "static/js/*",
        "static/html/*",
        "workspace/*"
    ]},
    # install_requires = ["vispa"],
)
