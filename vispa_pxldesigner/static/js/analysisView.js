define(["jquery", "emitter", "./plumbObjects", "./vendor/dom.jsPlumb-1.7.6-min"],
  function($, Emitter, plumbObjects) {

    var AnalysisView = Emitter._extend({

      init: function(instance, manager) {
        var self = this;

        this.instance = instance;
        this.manager = manager;
        this.server = instance.Server;

        this.viewNode = null;

        this.nodes = {};
        this.nodeNames = {}; //help array tp count the occurences

        this.connections = {};

        this.jsPlumb = null;

      },

      setup: function($node) {
        var self = this;

        this.viewNode = $node;
        //append module from file at init
        jsPlumb.ready(function() {
          //jsPlumb code goes here
          //documentation: https://jsplumbtoolkit.com/community/doc/home.html
          self.jsPlumb = jsPlumb.getInstance({
            container: "." + self.viewNode.attr("class")
          });

          self.jsPlumb.registerConnectionTypes({
            "basic": {
              paintStyle: { strokeStyle: "#428bca", lineWidth: 4 },
              hoverPaintStyle: { strokeStyle: "#f0ad4e", lineWidth: 5 }
            },
            "selected": {
              paintStyle: { strokeStyle: "#5bc0de", lineWidth: 5 },
              hoverPaintStyle: { strokeStyle: "#f0ad4e", lineWidth: 5 }
            }
          });

          self.jsPlumb.importDefaults({
            Connector: ["Flowchart", { cornerRadius: 50 }],
            ConnectionOverlays: [
              ["Arrow", {
                location: 0.8
              }],
              ["Label", {
                location: 0.1,
                id: "label"
              }]
            ]
          });
          if (self.instance.state.get("path") !== null)
            self.appendNodesFromFile();
          else {
            self.jsPlumb.bind("connectionDetached", function(info) {
              self.deleteConnection(info.connection.id, true);
            });
            self.jsPlumb.bind("connection", function(info) {
              if (info.targetId.indexOf("jsPlumb_") == -1) { //only then a sink/target is selected
                self.addConnection(info);
              }
            });
          }
        });

        //allow for appending new nodes from the module view
        this.viewNode.droppable({
          accept: ".moduleNodeSmall",
          drop: function(event, ui) {
            var moduleType = ($(ui.helper[0])).attr("data");
            //we must respect the scaling when adding a new node!
            var xPos = 100 * ui.position.left / self.instance.state.get("zoomLevel");
            var yPos = 100 * ui.position.top / self.instance.state.get("zoomLevel");
            var moduleName = self.createUniqueNodeName(moduleType);
            self.server.GET(["analysis", self.instance.state.get("ID"), "module", moduleName, moduleType], function(res) {
              self.appendNode(moduleName, {
                sinks: res.sinks,
                sources: res.sources,
                positions: {
                  x: xPos,
                  y: yPos
                },
                type: moduleType,
                enabled: res.enabled
              });
            });
            //exception applied here since adding a module is not a POST
            self.instance.state.set("modified", true);
          }
        }).click(function(event) {
          if ($(event.target).attr("class") == self.viewNode.attr("class")) {
            self.instance.state.set("selected", {
              type: "window",
              name: undefined
            });
            self.toggleNodes(null);
            self.toggleConnections(null);
          }
        });

        this.instance.on("changedState", function(key) {
          switch (key) {
            case "selected":
              var selected = self.instance.state.get("selected");
              if (selected.type == "module") {
                self.toggleNodes(selected.name);
                self.toggleConnections(null);
              } else if (selected.type == "connection") {
                self.toggleNodes(null);
                self.toggleConnections(selected.name);
              }
              break;
            case "focussed":
              if (self.instance.state.get("focussed")) {
                //allow the divs to be placed before drawing everything again
                setTimeout(function() {
                  self.repaintEverything()
                }, 50);
              };
              break;
            case "zoomLevel":
              var newZoomLevel = self.instance.state.get("zoomLevel");
              self.setZoomLevel(newZoomLevel);
              break;
            default:
              break;
          }
        });
      },

      appendNodesFromFile: function() {
        var self = this;
        this.server.GET(["analysis", this.instance.state.get("ID"), "modules"], function(res) {
          for (var i = 0; i < res.length; i++) {
            var name = res[i].name;
            self.createUniqueNodeName(name); //to increase the counter only
            self.appendNode(name, res[i]);
          }
          self.connectModules();
        });
      },

      appendNode: function(uniqueName, trunk) {
        var self = this;
        var tempModuleNode = new plumbObjects.moduleNode(self.instance, self, uniqueName, trunk);
        this.viewNode.append(tempModuleNode.jqueryNodeLarge());
        //the node must be placed first before calling the jsPlumb draggable function on it
        tempModuleNode.setZoomLevelManually(this.instance.state.get("zoomLevel"));
        tempModuleNode.setupDraggable();
        tempModuleNode.appendEndpoints();
        this.nodes[uniqueName] = tempModuleNode;
      },

      connectModules: function() {
        var self = this;
        this.server.GET(["analysis", self.instance.state.get("ID"), "connections"], function(res) {
          var connections = res;
          for (var i = 0; i < connections.length; i++) {
            var sinkModule = self.nodes[connections[i].sink_module];
            var sourceModule = self.nodes[connections[i].source_module];

            var connection = new plumbObjects.nodeConnection(self.instance, self, {
              "source": connections[i].source,
              "sink": connections[i].sink,
              "sourceModule": connections[i].source_module,
              "sinkModule": connections[i].sink_module,
              "sinkObject": sinkModule.getEndpointByName("sink", connections[i].sink),
              "sourceObject": sourceModule.getEndpointByName("source", connections[i].source)
            });

            var cID = connection.getID();
            self.connections[cID] = connection;

          }
          self.jsPlumb.bind("connectionMoved", function(trunk) {
            self.moveConnection(trunk);
          });
          self.jsPlumb.bind("connectionDetached", function(trunk) {
            self.deleteConnection(trunk.connection.id, true);
          });
          self.jsPlumb.bind("connection", function(trunk) {
            if (trunk.targetId.indexOf("jsPlumb_") == -1) { //only then a sink/target is selected
              self.addConnection(trunk);
            }
          });
        });
      },

      copyModule: function(sourcename, type) {
        var self = this;
        var targetname = self.createUniqueNodeName(type);
        self.server.GET(["analysis", self.instance.state.get("ID"), "module", sourcename, targetname, type], function(res) {
          self.appendNode(targetname, {
            sinks: res.sinks,
            sources: res.sources,
            positions: {
              x: self.nodes[sourcename].xPos+5,
              y: self.nodes[sourcename].yPos+5
            },
            type: type,
            enabled: res.enabled
          });
        })
      },

      createUniqueNodeName: function(name) {
        var numb = name.match(/[0-9]+/);
        if (numb !== null) {
          numb = numb[0];
          var tmpName = name.replace(numb, "");
          numb = parseInt(numb);
          this.nodeNames[tmpName] = this.nodeNames[tmpName] ? Math.max(this.nodeNames[tmpName], numb) : numb;
          return name;
        }
        if (this.nodeNames[name] === undefined)
          this.nodeNames[name] = 1;
        else
          this.nodeNames[name] += 1;
        return name + String(this.nodeNames[name]);
      },

      addConnection: function(info) {
        var connectionID = info.connection.id;
        if (this.connections[connectionID] !== undefined)
          return;
        var self = this,
          sourceModule = info.source.id.split("$TS=")[0],
          sinkModule = info.target.id.split("$TS=")[0],
          source = this.nodes[sourceModule].getEndpointNameByID("source", info.sourceEndpoint.id),
          sink = this.nodes[sinkModule].getEndpointNameByID("sink", info.targetEndpoint.id);

        this.server.POST(["analysis", this.instance.state.get("ID"), "connection", sourceModule, source, sinkModule, sink], function(res) {
          self.connections[connectionID] = new plumbObjects.nodeConnection(self.instance, self, {
            source: source,
            sink: sink,
            sourceModule: sourceModule,
            sinkModule: sinkModule,
            jsPlumbConnection: info.connection
          });
        });
      },

      deleteSelected: function() {
        var selected = this.instance.state.get("selected");
        if (selected.type == "module") {
          this.deleteNode(selected.name);
        } else if (selected.type == "connection") {
          this.externalDeleteConnection(selected.name);
        } else {
          return;
        }

        this.instance.state.set("selected", {
          type: "window",
          name: undefined
        });

      },

      moveConnection: function(trunk) {
        var connectionID = trunk.connection.id;

        var oldSource = this.connections[connectionID].source,
          oldSourceModule = this.connections[connectionID].sourceModule,
          oldSink = this.connections[connectionID].sink,
          oldSinkModule = this.connections[connectionID].sinkModule;

        var newSourceModule = trunk.newSourceId.split("$TS=")[0],
          newSinkModule = trunk.newTargetId.split("$TS=")[0],
          newSourceID = trunk.newSourceEndpoint.id,
          newSinkID = trunk.newTargetEndpoint.id,
          newSource = this.nodes[newSourceModule].getEndpointNameByID("source", newSourceID),
          newSink = this.nodes[newSinkModule].getEndpointNameByID("sink", newSinkID);

        //tell server
        this.server.DELETE(["analysis", this.instance.state.get("ID"), "connection", oldSourceModule, oldSource, oldSinkModule, oldSink]);
        this.server.POST(["analysis", this.instance.state.get("ID"), "connection", newSourceModule, newSource, newSinkModule, newSink]);
        //adjust connection (ID is the same)
        this.connections[connectionID].source = newSource,
          this.connections[connectionID].sink = newSink,
          this.connections[connectionID].sourceModule = newSourceModule,
          this.connections[connectionID].sinkModule = newSinkModule;
      },

      externalDeleteConnection: function(name) {
        if (name === undefined)
          return;
        this.jsPlumb.detach(this.connections[name].jsPlumbConnection);
      },

      deleteConnection: function(ID, tellServer) {
        var thisConnection = this.connections[ID];

        if (!thisConnection)
          return;

        delete this.connections[ID];

        if (tellServer) {
          var sourceModule = thisConnection.sourceModule,
            source = thisConnection.source,
            sinkModule = thisConnection.sinkModule,
            sink = thisConnection.sink;
          this.server.DELETE(["analysis", this.instance.state.get("ID"), "connection", sourceModule, source, sinkModule, sink]);
        }
      },

      deleteNode: function(name) {
        var self = this;
        if (name === undefined)
          return;

        //When removing the java script node, we only want to delete the connections only on the client side first
        //--> Need to overwrite the default callback.
        this.jsPlumb.unbind("connectionDetached");
        this.jsPlumb.bind("connectionDetached", function(info) {
          self.deleteConnection(info.connection.id, false);
        });
        this.delete$Node(name);

        //The workspace function already deletes all connections when removing a module.
        self.server.DELETE(["analysis", self.instance.state.get("ID"), "module", name], function(res) {
          //Reset the default callback when deleting a connection.
          self.jsPlumb.unbind("connectionDetached");
          self.jsPlumb.bind("connectionDetached", function(info) {
            self.deleteConnection(info.connection.id, true);
          });
        });
      },

      delete$Node: function(name) {
        this.jsPlumb.remove(name + this.instance.timeStamp);
        this.instance.state.set("module", undefined);
        this.nodes[name].hidePopover();
        delete this.nodes[name];
      },

      toggleNodes: function(name) {
        for (var key in this.nodes) {
          this.nodes[key].toggle(key == name);
        }
      },

      toggleConnections: function(ID) {
        for (var key in this.connections) {
          this.connections[key].toggle(key == ID);
        };
      },

      renameModule: function(old_name, new_name) {
        //loop over all nodes
        for (var key in this.nodes) {
          if (key != old_name)
            continue;
          this.nodes[new_name] = this.nodes[old_name];
          delete this.nodes[old_name];
        }

        //loop over all connections
        for (var key in this.connections) {
          var connection = this.connections[key];

          if (connection.sinkModule == old_name)
            connection.sinkModule = new_name;
          if (connection.sourceModule == old_name)
            connection.sourceModule = new_name;
        }
      },

      repaintEverything: function() {
        this.jsPlumb.repaintEverything();
      },

      setZoomLevel: function(newZoomLevel) {
        var self = this;
        this.viewNode.scrollTop(0);
        this.viewNode.scrollLeft(0);
        for (var key in self.nodes) {
          var node = self.nodes[key];
          node.setZoomLevelManually(newZoomLevel);
        }
        self.repaintEverything();

      }
    });

    return AnalysisView;
  });
