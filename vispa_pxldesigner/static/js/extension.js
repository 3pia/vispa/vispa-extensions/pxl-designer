define([
  "vispa/extension",
  "vispa/filehandler2",
  "vispa/views/main",
  "./ajax",
  "./prefs",
  "./viewManagement",
  "text!../html/main.html",
  "css!../css/styles",
], function(
  Extension,
  FileHandler2,
  MainView,
  AJAX,
  Prefs,
  ViewManagement,
  MainTemplate
) {

  var PXLDesignerExtension = Extension._extend({
    init: function init() {
      init._super.call(this, "pxldesigner", "PXL designer");
      this.mainMenuAdd([
        this.addView(PXLDesignerView),
      ]);
    }
  });


  var PXLDesignerView = MainView._extend({

    init: function init(args) {
      init._super.apply(this, arguments);
      var self = this;

      this.timeStamp = "$TS=" + String((new Date()).getTime()); //global timestamp to be added to each html elements ID

      this.setupState({
        path: undefined, //opened path
        selected: undefined, //name and type of selected element
        ID: undefined,
        modified: undefined,
        focussed: true,
        zoomLevel: 100
      }, args);

      //object each view owns:
      this.Server = new AJAX(this);
      this.ViewManagement = new ViewManagement(this);

      this.on("changedState", function(key, val) {
        switch (key) {
          case "modified":
            self.modified = self.state.get("modified");
            break;
        }
      });

      this.socket.on("watch", function(data) {
        if (data.watch_id != "pxlDesigner")
          return;
        if (data.event == "vanish") {
          self.confirm("File has been deleted or renamed. \n Do you want to save your process now? \n Otherwise the designer is closed.", function(res) {
            if (!res) {
              self.forceClose = true;
              self.close();
            } else {
              var callback = function() {
                self.spawnInstance("pxldesigner", "PXLDesigner", {
                  path: self.state.get("path"),
                  focussed: true
                });
                self.close();
              };
            }
          });
        }
      });
    },

    render: function($node) {
      var self = this;
      this.loading = true;

      var finalizeSetup = function(ID) {
        self.state.set("ID", ID);
        self.state.set("modified", false);
        self.state.set("selected", "window");
        $(MainTemplate).appendTo($node);
        self.ViewManagement.setup($node);
        self.loading = false;
      };
      if (self.state.get("path")) { //only start the reader if path is set{
        self.Server.GET(["analysis", self.state.get("path")], function(res) {
          finalizeSetup(res);
          self.setLabel(self.state.get("path"), true);
          self.POST("/ajax/fs/watch", {
            path: self.state.get("path"),
            watch_id: "pxlDesigner"
          });
        });
      } else {
        self.Server.GET(["analysis"], function(res) {
          self.state.set("path", null);
          finalizeSetup(res);
          self.setLabel("New analysis", false);
        });

      }
    },

    save: function(cb) {
      var self = this;
      if (this.state.get("path") !== null) {
        this.Server.POST(["analysis", this.state.get("ID"), "save", this.state.get("path")], function(res) {
          self.state.set("modified", false);
          if ($.isFunction(cb)) cb();
        });
      } else {
        self.openFileSelector(undefined, false, false, function(path) {
          if (path === null)
            return;
          self.state.set("path", path);
          self.Server.POST(["analysis", self.state.get("ID"), "save", self.state.get("path")], function(res) {
            if ($.isFunction(cb)) cb();
            self.setLabel(self.state.get("path"), true);
            self.POST("/ajax/fs/watch", {
              path: self.state.get("path"),
              watch_id: "pxlDesigner"
            });
          });
        });
      }
    },

    onFocus: function() {
      this.state.set("focussed", true);
      return this;
    },

    onBlur: function() {
      this.state.set("focussed", false);
      return this;
    },

    onBeforeClose: function() {
      if (this.modified) {
        var self = this;
        self.confirm("The analysis has been modified. Do you want to save our progress?", function(res) {
          if (!res)
            self.close(true);
          else {
            self.save(function() {
              self.close(true);
            });
          }
        });
      }

      return !this.modified;
    },

    createAnalysisSkeleton: function(moduleName) {
      var self = this;
      this.openFileSelector(null, false, false, function(path) {
        if (path === null) {
          return;
        } else if (path.indexOf(".py") == -1) {
          self.alert("Skeleton must be a .py file!");
          return;
        }
        var path_elements = path.split("/");
        var filename = path_elements.pop();
        var directory = path_elements.join("/");
        self.Server.POST(["analysis", self.state.get("ID"), "module", moduleName, "skeleton", directory, filename], function() {
          if (self.state.get("selected").name == moduleName) {
            self.emit("stateChange", "selected");
          }
          self.spawnInstance("codeeditor", "CodeEditor", {
            path: path
          });
        });
      });

    },

    openFileSelector: function(path, multimode, foldermode, callback) {
      var self = this;
      var args = {
        "path": path,
        "multimode": multimode,
        "foldermode": foldermode,
        callback: function(path) {
          if ($.isFunction(callback)) {
            callback(path);
          }
        },
        sort: "ext",
        reverse: false
      };
      this.spawnInstance("file2", "FileSelector", args);
    },
  },{
    iconClass: "fa-edit",
    label: "PXL Designer",
    name: "PXLDesignerView",
    menuPosition: 120,
    preferences: {
      items: Prefs.prefs,
    },
    menu: Prefs.menu,
    fastmenu: ["save"],
    fileHandlers:  [{
      label: "PXL Designer",
      iconClass: "fa-edit",
      position: 10,
      hidden: function () {
        var ext = this.value.split(".").pop().toLowerCase();
        return ext !== "xml";
      },
      callback: function () {
        (this.$root && this.$root.view || vispa).spawnInstance("pxldesigner", "PXLDesignerView",
          {path: this.value});
      }
    }],
  });

  FileHandler2.addOpen("pxldesigner", {
    label: "Pxl-Designer",
    iconClass: "fi-xml",
    position: 1,
  }, function (path) {
    return ["pxldesigner", "PXLDesignerView", {path: path}];
  }, ["xml"]);

  return PXLDesignerExtension;
});
