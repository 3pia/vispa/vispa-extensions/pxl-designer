define(["jquery", "emitter",
    "css!../css/moduleView"
  ],
  function($, Emitter) {
    var moduleNode = Emitter._extend({
      init: function(instance, view, name, trunk) {
        var self = this;
        this.instance = instance;
        this.view = view;
        this.server = instance.Server;

        //to store the connections etc
        this.ID = name + this.view.instance.timeStamp;
        this.name = name; //this is the module's name as shown on the GUI
        this.type = null;

        this.sources = null;
        this.sinks = null;
        this.xPos = null;
        this.yPos = null;

        this.enabled = null;

        this.$node = null;
        this.zoomLevel = 100;

        if (trunk !== undefined) {
          if (trunk.positions !== undefined) {
            this.xPos = trunk.positions.x;
            this.yPos = trunk.positions.y;
          }
          if (trunk.sources !== undefined)
            this.sources = trunk.sources;
          if (trunk.sinks !== undefined)
            this.sinks = trunk.sinks;

          if (trunk.type !== undefined)
            this.type = trunk.type;
          if (trunk.enabled !== undefined)
            this.enabled = trunk.enabled;
        }

      },

      jqueryNodeSmall: function() {
        var self = this;
        this.$node = $("<div/>")
          .html(this.name)
          .attr("data", this.name) //specific communication with the pxl reader
          .addClass("moduleNodeSmall")
          .draggable({
            appendTo: this.instance.ViewManagement.AnalysisView.viewNode,
            scroll: false,
            helper: "clone"
          });
        this.setOriginalWidth("small");
        return this.$node;
      },

      jqueryNodeLarge: function() {
        var self = this;

        this.$node = $("<div/>")
          .attr("data", this.name)
          .prop("id", this.ID)
          .addClass("moduleNodeLarge")
          .dblclick(function() {
            if (self.instance.state.get("selected").name != self.name)
              self.instance.state.set("selected", {
                type: "module",
                name: self.name
              });
          }).css({
            left: String(this.xPos) + "px",
            top: String(this.yPos) + "px"
          });
        this.$node.append($("<div/>").append("<i/>"))
        this.$node.append($("<span/>").html(this.name));


        this.makePopover();
        this.toggleEnable();
        this.setOriginalWidth("large");

        if (this.xPos !== undefined && this.yPos !== undefined)
          this.setPositionsAsUserRecord(true); //true==the state change is not emitted

        return this.$node;
      },

      setupDraggable: function() {
        var self = this;
        this.view.jsPlumb.draggable(this.name + this.instance.timeStamp, {
          stop: function(event) {
            var node = event.el;
            if (parseInt(node.style.top) < 0) {
              event.pos[1] = 0;
              node.style.top = "0px";
            }
            if (parseInt(node.style.left) < 0) {
              event.pos[0] = 0;
              node.style.left = "0px";
            }
            self.xPos = event.pos[0];
            self.yPos = event.pos[1];
            self.setPositionsAsUserRecord();
            self.view.repaintEverything();
          }
        });
      },

      makePopover: function() {
        var self = this;
        this.$node.find("i").popover({
            trigger: "manual",
            html: true,
            container: "body",
            animation: false,
            content: function() {
              var btns = $("<div/>").addClass("btn-group");

              //defaults:
              var enableButton = $("<button/>").addClass("btn btn-default")
                .append($("<i/>").addClass("glyphicon glyphicon-play").css("z-index", 2))
                .append("Enable/Disable").click(function() {
                  self.setEnable(!self.enabled);
                });
              btns.append(enableButton);

              var renameButton = $("<button/>").addClass("btn btn-default")
                .append($("<i/>").addClass("glyphicon glyphicon-pencil"))
                .append("Rename").click(function() {
                  self.rename();
                });
              btns.append(renameButton);

              var removeButton = $("<button/>").addClass("btn btn-default")
                .append($("<i/>").addClass("glyphicon glyphicon-trash"))
                .append("Remove")
                .click(function() {
                  self.view.deleteNode(self.name);
                });
              btns.append(removeButton);

              var reloadButton = $("<button/>").addClass("btn btn-default")
                .append($("<i/>").addClass("glyphicon glyphicon-refresh"))
                .append("Reload")
                .click(function() {
                  self.reload();
                });
              btns.append(reloadButton);

              var copyButton = $("<button/>").addClass("btn btn-default")
                .append($("<i/>").addClass("glyphicon glyphicon-copy"))
                .append("Copy")
                .click(function() {
                  self.view.copyModule(self.name, self.type);
                });
              btns.append(copyButton);

              if (self.type == "PyAnalyse") {
                var skeletonButton = $("<button/>").addClass("btn btn-default")
                  .append($("<i/>").addClass("glyphicon glyphicon-edit"))
                  .append("Create Skeleton")
                  .click(function() {
                    self.instance.createAnalysisSkeleton(self.name);
                  });
                btns.append(skeletonButton);
              }

              return btns;
            },
            placement: "auto"
          })
          .on("mouseenter", function() {
            var _this = this;
            $(this).popover("show");
            $(".popover").on("mouseleave", function() {
              $(_this).popover('hide');
            });
          }).on("mouseleave", function() {
            var _this = this;
            setTimeout(function() {
              if (!$(".popover:hover").length) {
                $(_this).popover("hide");
              }
            }, 50); //a delay is needed in order to move the mouse from the div to the popover
          });
      },

      hidePopover: function() {
        this.$node.find("i").popover("hide");
      },

      setZoomLevelManually: function(zoomLevel) {
        var self = this;
        var a;
        if (zoomLevel === undefined) {
          a = this.zoomLevel / 100.0;
        } else {
          zoomLevel = Math.round(zoomLevel);
          a = zoomLevel / this.zoomLevel;
          var oLeft = this.$node.position().left;
          var oTop = this.$node.position().top;
          var nLeft = a * oLeft;
          var nTop = a * oTop;
          this.$node.css({
            left: String(nLeft) + "px",
            top: String(nTop) + "px"
          });
        }
        var resizeElement = function(element) {
          $.each(element.children(), function(idx, child) {
            resizeElement($(child));
          });
          var oWidth = parseFloat(element.css("width"));
          var oHeight = parseFloat(element.css("height"));
          var oPadding = parseFloat(element.css("padding"));
          var oFontSize = parseFloat(element.css("font-size"));
          var oMargin = parseFloat(element.css("margin"));
          var oBorderRadius = parseFloat(element.css("border-radius"));

          var nWidth = a * oWidth;
          var nHeight = a * oHeight;
          var nPadding = a * oPadding;
          var nFontSize = a * oFontSize;
          var nMargin = a * oMargin;
          var nBorderRadius = a * oBorderRadius;

          element.css({
            width: String(nWidth) + "px",
            height: String(nHeight) + "px",
            fontSize: String(nFontSize) + "px",
            padding: String(nPadding) + "px",
            margin: String(nMargin) + "px",
            borderRadius: String(nBorderRadius) + "px"
          });
        }

        resizeElement(this.$node);
        this.zoomLevel = Math.round(zoomLevel);
      },

      setOriginalWidth: function(option) {
        var large = option == "large"
        var width = Math.max(large ? 120 : 100, this.name.length * (large ? 10 : 8));
        this.$node.css("width", width * (this.zoomLevel / 100.0) + "px");
        this.$node.find("*").css({
          width: "auto",
          height: "auto"
        });
      },

      setPositionsAsUserRecord: function(noModificationOfState) {
        var self = this;
        this.server.POST(["analysis", this.instance.state.get("ID"), "module", self.name, "userrecords", "xPos"], function(res1) {
          self.server.POST(["analysis", self.instance.state.get("ID"), "module", self.name, "userrecords", "yPos"], function(res2) {}, {
            value: Math.round(self.yPos * (100 / self.zoomLevel))
          }, noModificationOfState);
        }, {
          value: Math.round(self.xPos * (100 / self.zoomLevel))
        }, noModificationOfState);
      },

      reload: function() {
        var self = this;
        this.view.jsPlumb.detachAllConnections(self.$node);
        for (var i = 0; i < this.sources.length; i++) {
          this.view.jsPlumb.deleteEndpoint(this.sources[i].$object);
        }
        for (var i = 0; i < this.sinks.length; i++) {
          this.view.jsPlumb.deleteEndpoint(this.sinks[i].$object);
        }
        this.server.GET(["analysis", this.instance.state.get("ID"), "module", this.name], function(res) {
          self.sources = res.sources;
          self.sinks = res.sinks;
          self.appendEndpoints();
        });
      },

      rename: function(cb) {
        var self = this;

        this.instance.prompt("Enter a new (displayed) name for " + this.name, function(input) {
          var args = ["analysis", self.instance.state.get("ID"), "module", "rename", self.name, input];
          self.server.POST(args, function() {
            var oldName = self.name;
            self.name = input;
            self.view.renameModule(oldName, input);
            //replace the node ID and span
            var oldID = self.$node.attr("id");
            self.view.jsPlumb.setId(self.$node, oldID.replace(oldName, input));
            self.$node.find("span").html(input);
            //adjust the size and the graph
            self.setOriginalWidth("large");
            self.appendEndpoints();
            self.view.repaintEverything();

            if (self.instance.state.get("selected").name == oldName) {
              self.instance.state.set("selected", {
                type: "module",
                name: input
              });
            }
          });
          if ($.isFunction(cb)) cb(input);
        }, {
          defaultValue: this.name
        });
      },

      appendEndpoints: function() {
        this.view.jsPlumb.removeAllEndpoints(this.$node);

        var sinkAnchors = ["Left"];
        for (var i = 0; i < this.sinks.length; i++) {

          var sinkEndpoint = this.view.jsPlumb.addEndpoint(this.$node, {
            anchor: sinkAnchors[i],
            connectionType: "basic",
            paintStyle: { fillStyle: "#5cb85c" }
          }, {
            isTarget: true,
            overlays: [
              ["Label", {
                location: [0.5, -0.5],
                label: this.sinks[i].name,
                cssClass: "endpointLabel"
              }]
            ],
            maxConnections: -1
          });
          this.sinks[i]["anchor"] = sinkAnchors[i]; //append the anchor
          this.sinks[i]["endpointID"] = sinkEndpoint.id;
          this.sinks[i]["$object"] = sinkEndpoint;
        }

        var sourceAnchors = this.getSourceAnchors(this.sources.length);

        for (var i = 0; i < this.sources.length; i++) {
          var sourceEndpoint = this.view.jsPlumb.addEndpoint(this.$node, {
            anchor: sourceAnchors[i],
            connectionType: "basic",
            paintStyle: { fillStyle: "#777" }
          }, {
            isSource: true,
            overlays: [
              ["Label", {
                location: [0.5, 1.5],
                label: this.sources[i].name,
                cssClass: "endpointLabel"
              }]
            ]
          });
          this.sources[i]["anchor"] = sourceAnchors[i]; //append the anchor
          this.sources[i]["endpointID"] = sourceEndpoint.id;
          this.sources[i]["$object"] = sourceEndpoint;
        }
      },
      
      getSourceAnchors: function(N) {
        switch (N) {
          case 1:
            return ["Right"];
          case 2:
            return ["TopRight", "BottomRight"];
          default:
            return ["Right", "BottomRight", "TopRight", "Bottom", "Top", "TopLeft", "BottomLeft"].slice(0, N);
        }
      },

      getEndpointNameByID: function(type, ID) {
        var map = null;
        switch (type) {
          case ("source"):
            map = this.sources;
            break;
          case ("sink"):
            map = this.sinks;
            break;
          default:
            return undefined;
        }
        for (var i = 0; i < map.length; i++) {
          if (map[i].endpointID == ID)
            return map[i].name;
        }
        return undefined;
      },

      getEndpointByName: function(type, name) {
        var map = null;
        switch (type) {
          case ("source"):
            map = this.sources;
            break;
          case ("sink"):
            map = this.sinks;
            break;
          default:
            return undefined;
        }
        for (var i = 0; i < map.length; i++) {
          if (map[i].name == name) {
            return map[i].$object;
          }
        }
        return undefined;
      },

      toggle: function(toggled) {
        if (toggled)
          this.$node.addClass("toggled");
        else
        if (this.$node.hasClass("toggled"))
          this.$node.removeClass("toggled");
      },

      toggleEnable: function(_enable) {
        var enabled = (_enable === undefined) ? this.enabled : _enable;
        var icon = this.$node.find("i");
        var node = this.$node;

        var iconClass = null,
          nodeClass = null,
          iconClass_remove = null,
          nodeClass_remove = null;

        if (enabled) {
          iconClass = "glyphicon glyphicon-edit",
            nodeClass = "Enabled",
            iconClass_remove = "glyphicon glyphicon-pause",
            nodeClass_remove = "Disabled";
        } else {
          iconClass = "glyphicon glyphicon-pause",
            nodeClass = "Disabled",
            iconClass_remove = "glyphicon glyphicon-edit",
            nodeClass_remove = "Enabled";
        }

        if (node.hasClass(nodeClass_remove))
          node.removeClass(nodeClass_remove);
        if (icon.hasClass(iconClass_remove))
          icon.removeClass(iconClass_remove);

        node.addClass(nodeClass);
        icon.addClass(iconClass);
      },

      setEnable: function(enabled) {
        var self = this;
        this.server.POST(["analysis", this.instance.state.get("ID"), "module", this.name, enabled], function() {
          self.enabled = enabled;
          self.toggleEnable();
          if (self.instance.state.get("selected").name == self.name) {
            self.instance.emit("stateChange", "selected")
          }
        });
      }
    });


    var nodeConnection = Emitter._extend({
      init: function(instance, view, trunk) {
        var self = this;
        this.instance = instance;
        this.view = view;
        this.jsPlumb = view.jsPlumb;

        this.timeStamp = instance.timeStamp;

        this.sink = null;
        this.source = null;
        this.sinkModule = null;
        this.sourceModule = null;
        this.sinkObject = null;
        this.sourceObject = null;
        this.jsPlumbConnection = null;
        this.jsPlumbId = null;

        this.wasSelected = false;

        //TODO: cleanup, remove all unnecessary stuff
        if (trunk.sink !== undefined)
          this.sink = trunk.sink;
        if (trunk.source !== undefined)
          this.source = trunk.source;
        if (trunk.sinkModule !== undefined)
          this.sinkModule = trunk.sinkModule;
        if (trunk.sourceModule !== undefined)
          this.sourceModule = trunk.sourceModule;
        if (trunk.sinkObject !== undefined)
          this.sinkObject = trunk.sinkObject;
        if (trunk.sourceObject !== undefined)
          this.sourceObject = trunk.sourceObject;
        if (trunk.jsPlumbConnection !== undefined)
          this.jsPlumbConnection = trunk.jsPlumbConnection;
        else {
          this.jsPlumbConnection = this.jsPlumb.connect({
            source: this.sourceObject,
            target: this.sinkObject
          });
        }
        this.jsPlumbId = $(this.jsPlumbConnection).attr("id");

        this.jsPlumbConnection.bind("click", function() {
          self.instance.state.set("selected", {
            type: "connection",
            name: self.jsPlumbId
          });
        });

      },

      toggle: function(selected) {
        if (selected) {
          this.jsPlumbConnection.toggleType("basic");
          this.jsPlumbConnection.toggleType("selected");
          this.wasSelected = true;
        } else if (this.wasSelected) {
          this.jsPlumbConnection.toggleType("selected");
          this.jsPlumbConnection.toggleType("basic");
          this.wasSelected = false;
        }


      },

      getID: function() {
        return this.jsPlumbId;
      }
    });

    return {
      moduleNode: moduleNode,
      nodeConnection: nodeConnection
    }

  });
