define(["jquery", "emitter"],
  function($, Emitter) {

    var Server = Emitter._extend({
      init: function(instance) {
        var self = this;

        this.instance = instance;
      },

      GET: function(urlArray, cb, args) {
        var self = this.instance;
        self.GET(urlArray, JSON.stringify(args), function(err, res) {
          if (err)
            return;
          else {
            if ($.isFunction(cb))
              cb(res);
          }
        });
      },

      POST: function(urlArray, cb, args, noModification) {
        var self = this.instance;
        self.POST(urlArray, JSON.stringify(args), function(err, res) {
          if (err)
            return;
          else {
            if (!noModification) {
              self.state.set("modified", true);
            }
            if ($.isFunction(cb))
              cb(res);
          }
        });
      },

      DELETE: function(urlArray, cb, args, noModification) {
        var self = this.instance;
        self.DELETE(urlArray, JSON.stringify(args), function(err, res) {
          if (err)
            return;
          else {
            if (!noModification) {
              self.state.set("modified", true);
              if ($.isFunction(cb))
                cb(res);
            }
          };
        });
      }
    });
    return Server;
  });
