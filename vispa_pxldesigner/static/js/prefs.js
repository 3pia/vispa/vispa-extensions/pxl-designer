define(function() {

  var metaCtrl = function (key) {
    return "mac:meta+"+key+" ctrl+"+key;
  };

  var prefs = {
    save: {
      type: "shortcut",
      level: 2,
      description: "Save the analysis.",
      value: metaCtrl("s"),
      callback: function() {
        this.save();
      },
    },
    delete: {
      type: "shortcut",
      level: 2,
      description: "Delete selected items..",
      value: "max:meta+backspace del",
      callback: function() {
        this.ViewManagement.AnalysisView.deleteSelected();
      },
    }
  };

  var menu = {
    save: {
      label: "Save",
      iconClass: "glyphicon glyphicon-floppy-disk",
      buttonClass: "btn-default",
      callback: function() {
        this.$root.instance.save();
      },
    },
    execute: {
      label: "Execute / Abort",
      iconClass: "glyphicon glyphicon-play",
      buttonClass: "btn-default",
      callback: function() {
        this.$root.instance.ViewManagement.OutputView.execute();
      },
    },
    remove: {
      label: "Remove",
      iconClass: "glyphicon glyphicon-trash",
      buttonClass: "btn-default",
      callback: function() {
        this.$root.instance.ViewManagement.AnalysisView.deleteSelected();
      },
    },
    Zoom: {
      label: "Zoom",
      iconClass: "glyphicon glyphicon-search",
      buttonClass: "btn-default",
      callback: function() {
        var view = this.$root.instance;
        view.prompt("Set the new zoom level [%]", function(res) {
          var newZoomLevel = parseFloat(res);
          view.state.set("zoomLevel", newZoomLevel);
        }, {
          value: view.state.get("zoomLevel")
        });
      },
    },
    designer: {
      label: "Open in Jobdesigner",
      iconClass: "glyphicon glyphicon-pencil",
      buttonClass: "btn-default",
      callback: function() {
        var view = this.$root.instance;
        view.Server.GET(["analysis", self.state.get("ID"), "modules", "options"], function(res) {
          view.spawnInstance("jobmanagement","designerCenter",{options:res});
        });
      },
    },
  };

  return {
    prefs: prefs,
    menu: menu,
  };

});
