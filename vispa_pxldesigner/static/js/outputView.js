define(["jquery", "emitter"],
  function($, Emitter) {

    var OutputView = Emitter._extend({

      init: function(instance, manager) {
        var self = this;

        this.instance = instance;
        this.manager = manager;
        this.server = instance.Server;

        this.viewNode = null;

        this.textSet = false;
        this.temporaryText = "";
        this.appendInterval = null;

        this.failedExecuteTimeOut = null;

        this.preNode = null;
        this.running = false; //execute command has been fired?
        this.hasStarted = false; //stream in WS has started?
      },

      setup: function(node) {
        var self = this;

        this.preNode = $("pre", node);

        // pre scroll/clear
        $("button.clear", node).click(function(event) {
          self.clear();
          this.blur();
          event.preventDefault();
        });
        $("button.gototop", node).click(function(event) {
          self.goToTop();
          this.blur();
          event.preventDefault();
        });
        $("button.gotobottom", node).click(function(event) {
          self.goToBottom();
          this.blur();
          event.preventDefault();
        });

        // check if there is a running job
        this.server.GET(["analysis", this.instance.state.get("ID"), "execution"], function(res) {
          self.running = !!res.running;
        });


        // register events
        this.instance.socket.on("start", function(data) {
          clearTimeout(self.failedExecuteTimeOut);
          self.failedExecuteTimeOut = null;
          self.running = true;
          self.hasStarted = true;
          var autoClear = true; //hard coded still
          self.textSet = self.textSet && !autoClear;
          self.append(
            self.getDate() + ": \n" +
            "executing \"" + data.command + "\"\n" +
            "--------------------\n" +
            "OUTPUT:\n\n");
          self.appendInterval = setInterval(function() {
            self.append();
          }, 500);
        });
        this.instance.socket.on("data", function(data) {
          self.attachToTemporary(data);
        });
        this.instance.socket.on("done", function(data) {
          clearInterval(self.appendInterval);
          self.appendInterval = null;
          if (self.hasStarted) {
            self.append(); //append the remaining temporaries
            self.append(
              "\n--------------------\n" +
              (data.aborted ? "process was aborted!\n" : "") +
              "runtime: " + self.formatRuntime(data.runtime) + "\n" +
              "====================\n\n\n");
          }
          self.running = false;
          self.hasStarted = false;
        });

        return this;
      },

      formatRuntime: function(t) {
        t = parseFloat(t);
        var days = Math.floor(t / 86400);
        t -= days * 86400;
        var hours = Math.floor(t / 3600);
        t -= hours * 3600;
        var minutes = Math.floor(t / 60);
        t -= minutes * 60;
        t = t.toFixed(2);
        var s;
        if (days)
          s = days + "d, " + hours + " h, " + minutes + " m, " + t + " s";
        else if (hours)
          s = hours + " h, " + minutes + " m, " + t + " s";
        else if (minutes)
          s = minutes + " m, " + t + " s";
        else
          s = t + " s";
        return s;
      },

      getDate: function() {
        var d = new Date();

        return d.toLocaleString();
      },

      // output
      setText: function(text) {
        if (!text) {
          this.preNode.text(text);
          return this;
        }
        var textarray = text.split("\n");
        //maximum number of lines is 2000 for now
        var maxlines = 2000 < textarray.length ?
          2000 : textarray.length;
        var cleantext = textarray.slice(-maxlines).join("\n");
        this.preNode.text(cleantext);
        return this;
      },

      getText: function() {
        return this.preNode.text();
      },

      attachToTemporary: function(text) {
        this.temporaryText = this.temporaryText + String(text);
      },

      append: function(text) {
        this.initialClear();

        var wasAtBottom = this.isAtBottom();
        this.setText(this.getText() + String(text === undefined ? this.temporaryText : text));
        this.temporaryText = "";
        if (wasAtBottom)
          this.goToBottom();

        return this;
      },

      clear: function() {
        this.setText("");
        return this;
      },

      initialClear: function() {
        if (!this.textSet) {
          this.textSet = true;
          this.clear();
        }
        return this;
      },

      goTo: function(offset) {
        if (this.preNode)
          this.preNode.scrollTop(offset);
        return this;
      },

      goToTop: function() {
        this.goTo(0);
        return this;
      },

      goToBottom: function() {
        var offset = this.preNode.get(0).scrollHeight - this.preNode.innerHeight();
        this.goTo(offset);
        return this;
      },

      isAtBottom: function() {
        var offset = this.preNode.get(0).scrollHeight - this.preNode.innerHeight() - 5;
        return offset <= this.preNode.scrollTop();
      },

      abort: function() {
        if (!this.running)
          return;
        this.server.DELETE(["analysis", this.instance.state.get("ID"), "execution"], null, null, true);
      },

      execute: function() {
        var self = this;
        if (this.instance.state.get("path") === null) {
          this.instance.alert("Please save the analysis as file prior to execution!");
          return;
        }

        if (this.running) {
          this.instance.confirm("Abort the currently running job ?", function(res) {
            if (res)
              self.abort();
          });
          return;
        }

        var executeCall = function() {
          var path = self.instance.state.get("path").split("/");
          var cmd = "pxlrun " + path.pop();
          path = path.join("/");
          self.server.POST(["analysis", self.instance.state.get("ID"), "execution", cmd, path], null, null, true);
          self.manager.DragBars["mb"].toggle(true);
        }

        if (this.instance.modified) {
          this.instance.confirm("Do you want to save the modified analysis prior to execution?", function(res){
            if (!res)
              executeCall();
            else {
              self.instance.save(executeCall);
            }
          })
        } else
          executeCall();
      }
    });


    return OutputView;

  });
