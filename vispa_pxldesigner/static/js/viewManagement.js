define(["jquery", "emitter", "./analysisView", "./moduleView", "./outputView", "./propertyView"],
  function($, Emitter, AnalysisView, ModuleView, OutputView, PropertyView) {

    var ViewManager = Emitter._extend({
      init: function(instance) {
        var self = this;

        this.instance = instance;
        //the node that contains all windows
        this.mainNode = null;

        this.AnalysisView = new AnalysisView(instance, this);
        this.ModuleView = new ModuleView(instance, this);
        this.OutputView = new OutputView(instance, this);
        this.PropertyView = new PropertyView(instance, this);
        this.DragBars = {};
      },

      setup: function($node) {
        var self = this;
        this.mainNode = $node;

        this.AnalysisView.setup($node.find(".middle-center"));
        this.ModuleView.setup($node.find(".middle-left"));
        this.OutputView.setup($node.find(".bottom"));
        this.PropertyView.setup($node.find(".middle-right"));
        this.DragBars["mb"] = new DragBar($node.find(".drag-mb"), $node.find(".middle"), $node.find(".bottom"), false);
        this.DragBars["lc"] = new DragBar($node.find(".drag-lc"), $node.find(".middle-left"), $node.find(".middle-center"), true);
        this.DragBars["cr"] = new DragBar($node.find(".drag-cr"), $node.find(".middle-center"), $node.find(".middle-right"), true);

        $(window).on("resize", function() {
          for (var key in self.DragBars) {
            self.DragBars[key].rePosition();
          }
        });

        this.instance.on("changedState", function(key) {
          switch (key) {
            case "selected":
              if (self.instance.state.get("selected").type == "module") {
                self.DragBars["cr"].toggle(true);
              }
              break;
          }
        });
      }
    });

    var DragBar = Emitter._extend({
      init: function(barDiv, view1, view2, horizontal) {
        this.barDiv = barDiv;
        this.view1 = view1;
        this.view2 = view2;
        this.horizontal = horizontal;
        this.oldDragPosition = null;
        this.cssProperty = horizontal ? "left" : "top";
        this.cssPropertyBar = this.cssProperty == "left" ? "right" : "bottom";

        this.caret = null;
        this.toggled = false;

        //set general css properties
        this.barDiv.css({
          position: "absolute",
          zIndex: "1",
          overflow: "visible",
          backgroundColor: "grey"
        });
        if (this.horizontal)
          this.barDiv.css({
            width: "2px",
            height: "100%",
            cursor: "ew-resize"
          });
        else
          this.barDiv.css({
            height: "2px",
            width: "100%",
            cursor: "ns-resize"
          });

        //add the dragging functionality
        var self = this;
        this.barDiv.draggable({
          axis: horizontal ? "x" : "y",
          start: function(event, ui) {
            self.oldDragPosition = self.barDiv.css(self.cssProperty);
          },
          stop: function(event, ui) {
            self.arrangeNeighbors();
          }
        });

        this.setupCaret();
      },

      arrangeNeighbors: function() {
        var r = 0;
        if (this.horizontal) {
          r = 100.0 * this.barDiv.position().left / $(this.barDiv).parent().width();
        } else
          r = 100.0 * this.barDiv.position().top / $(this.barDiv).parent().height();
        //prevent overflow
        var noOverlap = null;
        if (this.horizontal) {
          noOverlap = this.barDiv.position().left < this.view2.position().left + this.view2.width();
          noOverlap = noOverlap && this.barDiv.position().left > this.view1.position().left;
        } else {
          noOverlap = this.barDiv.position().top < this.view2.position().top + this.view2.height();
          noOverlap = noOverlap && this.barDiv.position().top > this.view1.position().top;
        }
        if (r > 100.0 || !noOverlap) {
          this.barDiv.css(this.horizontal ? "left" : "top", this.oldDragPosition);
          return;
        } else {
          r1 = String(100 - r) + "%";
          r2 = String(r) + "%";
          this.view1.css(this.cssPropertyBar, r1);
          this.view2.css(this.cssProperty, r2);
        }
      },

      setupCaret: function() {
        var self = this;
        this.caret = this.barDiv.find("span").css({
          position: "absolute",
          cursor: "pointer"
        });

        if (this.horizontal) {
          this.caret.css({
            top: "50%"
          });
          if (this.view1.hasClass("middle-center")) {
            //right caret
            this.caret.css("right", "0px");
          } else {
            //left caret
            this.caret.css("left", "0px");
          }
        } else {
          //this is middle-bottom
          this.caret.css({
            left: "50%",
            bottom: "0px"
          });
        }

        this.caret.click(function() {
          self.toggle();
        });
      },

      toggle: function(toggled) {
        if (this.toggled == toggled) {
          return;
        }

        if (this.horizontal) {
          this.caret.find("i").toggleClass("glyphicon glyphicon-menu-left");
          this.caret.find("i").toggleClass("glyphicon glyphicon-menu-right");


          if (!this.view1.hasClass("middle-center")) { //this is the right
            if (this.caret.find("i").hasClass("glyphicon-menu-left")) {
              leftValue = "15%";
              this.view1.css("overflow", "auto");
              this.toggled = true;
            } else {
              leftValue = "0.1%";
              this.view1.css("overflow", "hidden");
              this.toggled = false;
            }
          } else {
            if (this.caret.find("i").hasClass("glyphicon-menu-right")) {
              leftValue = "70%";
              this.view2.css("overflow", "auto");
              this.toggled = true;
            } else {
              leftValue = "99.9%";
              this.view2.css("overflow", "hidden");
              this.toggled = false;
            }
          }
          this.rePosition({
            left: leftValue
          });
        } else {
          this.caret.find("i").toggleClass("glyphicon glyphicon-menu-up");
          this.caret.find("i").toggleClass("glyphicon glyphicon-menu-down");

          var topValue = null;
          if (this.caret.find("i").hasClass("glyphicon-menu-down")) {
            topValue = "50%";
            this.view2.css("overflow", "auto");
            this.toggled = true;
          } else {
            topValue = "99.9%";
            this.view2.css("overflow", "hidden");
            this.toggled = false;
          }
          this.rePosition({
            top: topValue
          });
        }
      },

      rePosition: function(cssArgs) {
        if (cssArgs !== undefined)
          this.barDiv.css(cssArgs);
        this.arrangeNeighbors();
      }
    });

    return ViewManager;
  });
