define(["jquery", "emitter", "text!../html/newUR.html"],
  function($, Emitter, newURTmpl) {

    function cast_to_pxltype(input, clickedType) {
      switch (clickedType) {
        case "String":
        case "InputFile":
        case "OutputFile":
          input = String(input);
          break;
        case "StringVector":
        case "InputFileVector":
        case "OutputFileVector":
          input = input.split(",");
          break;
        case "Boolean":
          input = Boolean(input);
          break;
        case "Long":
          input = parseInt(input);
          break;
        case "Double":
          input = parseFloat(input);
          break;
        default:
          input = input;
      }
      return input;
    }

    function formatTableHeader(title) {
      switch (title) {
        case ("userrecords"):
          return "User Records";
        case ("maindata"):
          return "Main";
        case ("options"):
          return "Options";
        case ("meta"):
          return "Meta";
        case ("plugin_paths"):
          return "Plugin Paths";
        case ("python_path"):
          return "Python Path";
        case ("search_paths"):
          return "Search Paths";
        default:
          return title;
      }
    }

    var stringVectorWindow = {
      init: function(owner, vecString) {
        this.owner = owner;
        this.originalContent = vecString.split(",");

        this.$node = $("<table/>").css("width", "100%").addClass("table table-hover");
        this.$body = null;
        this.selected = null;
        var lastEntry = this.originalContent[this.originalContent.length - 1];
        //setting up the node
        this.$node.append($("<thead/>")
          .append($("<th/>").html("Value"))
          .append($("<th/>").html("Select"))
          .append($("<th/>").html("Remove"))
        );
        this.$node.append($("<tbody/>"));
        this.$body = this.$node.find("tbody");

        for (var i = 0; i < this.originalContent.length - 1; i = i + 1) {
          var _entry = this.originalContent[i];
          this.addEntry(_entry);
          if (_entry == lastEntry) {
            this.select(_entry);
          }
        }
        if (this.selected === null && lastEntry) {
          this.addEntry(lastEntry);
        }

        this.appendAddLine();
      },

      addEntry: function(_entry, prepend) {
        if (prepend === undefined) prepend = false;
        var self = this;
        var $entry = $("<tr/>").attr("data-content", _entry)

        $entry.append($("<td/>").html(_entry));

        $entry.append($("<td/>").append($("<button/>").addClass("btn btn-default")
          .append($("<i/>").addClass("glyphicon glyphicon-remove").attr("id", "toggleIcon"))
          .click(function() {
            self.select($(this).parent().parent().attr("data-content"));
          })));

        $entry.append($("<td/>").append($("<button/>").addClass("btn btn-danger")
          .append($("<i/>").addClass("glyphicon glyphicon-trash"))
          .click(function() {
            self.clearSelection($(this).parent().parent().attr("data-content"));
            $(this).parent().parent().remove();
          })));

        if (prepend)
          this.$node.prepend($entry);
        else
          this.$node.append($entry);
      },

      select: function(_selected) {
        var self = this;
        this.selected = (this.selected == _selected) ? null : _selected;

        this.$body.children().each(function() {
          if ($(this).attr("id") == "add")
            return;
          var iconClass = "glyphicon glyphicon-";
          if ($(this).attr("data-content") == self.selected) {
            iconClass += "ok";
          } else {
            iconClass += "remove";
          }

          $(this).find("#toggleIcon").removeClass();
          $(this).find("#toggleIcon").addClass(iconClass);
        });

      },

      clearSelection: function() {
        this.selected = null;
      },

      getNode: function() {
        return this.$node;
      },

      appendAddLine: function() {
        var self = this;
        var $lastLine = $("<div/>").addClass("form-group");
        $lastLine.append($("<label/>").attr("for", "add").html("Add:"));
        $lastLine.append($("<input/>").attr("type", "text").attr("id", "add").addClass("form-control"));
        $lastLine.append($("<button/>").addClass("btn btn-success")
          .append($("<i/>").addClass("glyphicon glyphicon-plus"))
          .click(function() {
            var text = $(this).parent().find("input").val();
            if (!text) return;
            self.addEntry(text, prepend = true);
          }));
        this.$node.append($lastLine);
      },

      getVector: function() {
        var newEntryVec = [];
        this.$body.children().each(function() {
          if ($(this).attr("id") == "add")
            return;
          newEntryVec.push($(this).attr("data-content"));
        });

        if (this.selected !== null)
          newEntryVec.push(this.selected);
        return newEntryVec;
      }
    }



    var PropertyView = Emitter._extend({

      init: function(instance, manager) {
        var self = this;

        this.instance = instance;
        this.manager = manager;
        this.server = instance.Server;

        this.viewNode = null;
        this.timestamp = "STAMP" + String((new Date()).getTime());

        this.selectedName = null;
        this.selectedType = null;

      },

      setup: function($node) {
        var self = this;
        this.viewNode = $node;
        this.instance.on("changedState", function(key) {
          switch (key) {
            case "selected":
              self.selectedType = self.instance.state.get("selected").type;
              if (self.selectedType != "connection") {
                self.update();
              }
              break;
          }
        });

        this.update();
      },

      reset: function() {
        if (this.viewNode)
          this.viewNode.html("");
        return this;
      },

      update: function() {
        var self = this;
        var selected = this.instance.state.get("selected");
        if (selected !== undefined) {
          var type = selected.type;
          var name = selected.name;
          this.selectedName = (type == "module") ? name : undefined;
        }

        this.reset();
        var args = null,
          args2 = null;
        if (type != "module" && this.instance.state.get("path")) {
          args = ["analysis", self.instance.state.get("ID")];
          args2 = ["analysis", self.instance.state.get("ID"), "env"];
        } else if (name && type == "module") {
          args = ["analysis", self.instance.state.get("ID"), "module", name];
        } else {
          args = ["analysis", self.instance.state.get("ID"), "env"];
        }
        this.server.GET(args, function(res) {
          if (args2 === null)
            self.generateView(self.selectedName, res);
          else {
            self.server.GET(args2, function(res2) {
              self.generateView(self.selectedName, $.extend(res2, res));
            });
          }
        })
      },


      generateView: function(internalName, data) {
        var self = this;
        $.each(data, function(key, entry) {
          if (["userrecords", "options", "plugin_paths", "python_path", "search_paths"].indexOf(key) == -1)
            return;
          self.makeBlock(key, entry);
        });
        $.each(data, function(key, entry) {
          if (["maindata", "meta"].indexOf(key) == -1)
            return;
          self.makeBlock(key, entry);
        });
        this.makeHeader(internalName);
      },

      makeHeader: function(internalName) {
        var self = this;
        this.viewNode.remove("h1");

        var path = this.instance.state.get("path");
        if (internalName) {
          var headerText = this.manager.AnalysisView.nodes[internalName].name;
        } else if (path) {
          var headerText = path.split("/").pop();
        } else {
          var headerText = "New analysis";
        }

        var headerNode = $("<h1/>").addClass("pxldesigner_data_header")
          .append($("<span/>").attr("ID", "title").text(headerText));

        if (this.selectedType == "module") {
          headerNode.click(function() {
            self.manager.AnalysisView.nodes[self.selectedName].rename();
          });
          headerNode.css("cursor", "pointer");
        }
        headerNode.prependTo(this.viewNode);
      },

      makeBlock: function(key, entry) {
        var self = this;

        var values = entry; //array containing all possible values
        var context = key;
        //create some divs
        var outer = $("<div />").addClass("pxldesigner_gridpanel")
          .prependTo(this.viewNode);
        $("<a />").addClass("pxldesigner_gridpanel_label")
          .append(formatTableHeader(context)).attr({
            "href": "#" + context + this.timestamp,
            "data-toggle": "collapse",
            "area-expanded": true
          }).appendTo(outer);

        //here the tables are made
        var grid = this.makeTable(values, context);
        if (grid.tableDiv) {
          $(grid.tableDiv).appendTo(outer);
          if (context == "userrecords") {
            grid.tableDiv.append(
              $("<button />")
              .addClass("btn btn-default")
              .html("Add " + formatTableHeader(context))
              .click(function() {
                self.addUserRecord(grid.bodyDiv, context);
              })
              .prepend(
                $("<i />").addClass("glyphicon glyphicon-plus")
              )
            );
          }

          if (context.indexOf("_path") !== -1) {
            grid.tableDiv.append(
              $("<button />")
              .addClass("btn btn-default")
              .html("Add " + formatTableHeader(context))
              .click(function() {
                self.addPath(grid.bodyDiv, context);
              })
              .prepend(
                $("<i />").addClass("glyphicon glyphicon-plus")
              )
            );
          }
        }
      },

      makeTable: function(data, context) {
        var self = this;

        var tableDiv = $("<div />").addClass("collapse in").attr("id", context + self.timestamp);
        var table = $("<table />").addClass("table table-hover table-condensed")
          .appendTo(tableDiv);

        var head = $("<tr />").appendTo($("<thead />")).appendTo(table);

        head.append($("<th />").html("Property"));
        head.append($("<th />").html("Value"));
        var body = $("<tbody />").appendTo(table);

        //check if data exists
        if (!(!$.isArray(data) || !data.length)) {
          for (var i = 0; i < data.length; i++) {
            entry = data[i];
            if (typeof(entry) == "object") {
              var title = entry["description"] !== undefined ? entry["description"] : entry["type_name"];
              var label = entry["label"] !== undefined ? entry["label"] : entry["key"];
              var value = entry["value"] !== undefined ? entry["value"] : "";
              var type = entry["type_name"] !== undefined ? entry["type_name"] : "";
            } else {
              var title = "";
              var label = String(i);
              var value = String(entry);
            }

            var row = this.getRow({
              label: label,
              value: value,
              type: type,
              title: title,
              context: context
            }).appendTo(body);
          };
        }

        return {
          tableDiv: tableDiv,
          bodyDiv: body
        };
      },

      getRow: function(attr) {
        var self = this;
        var row = $("<tr />")
          .attr("data-content", String(cast_to_pxltype(attr.value, attr.type)))
          .attr("data-label", String(attr.label))
          .attr("data-type", attr.type);
        row.append(
          $("<td />").attr("id", "key")
          .attr("title", attr.title)
          .html(String(attr.label))
        );
        if (attr.type == "string_vector" && !(attr.label == "filename" || attr.label == "File names")) {
          var value = attr.value;
          if (value.indexOf(value[value.length - 1]) != value.length - 1) {
            value = value.pop();
          }

          row.append(
            $("<td />").attr("id", "value")
            .html(String(value))
          )
        } else {
          row.append(
            $("<td />").attr("id", "value")
            .html(String(cast_to_pxltype(attr.value, attr.type)))
          );
        }

        //changing the entries:
        if (["userrecords", "options"].indexOf(attr.context) != -1)
          row.find("#value").click(function() {
            self.editEntry($(this).parent(), attr.context);
          });

        //delete user records
        if (attr.context == "userrecords") {
          row.append(
            $("<i />").addClass("glyphicon glyphicon-remove")
            .css("cursor", "pointer")
            .click(function() {
              self.deleteEntry($(this).parent(), attr.context);
            })
          );
        }

        return row;
      },

      addUserRecord: function(table, context) {
        var self = this;

        var dialog = null;
        //setup of external template

        var $body = $(newURTmpl).find("#body");
        var $footer = $(newURTmpl).find("#footer");
        var apply = false;
        $footer.find("#done").click(function() {
          apply = true;
          dialog.close();
        });
        $footer.find("#cancel").click(function() {
          dialog.close();
        });


        this.instance.dialog({
          header: "<i class='glyphicon glyphicon-info-sign'></i>" +
            "Add a new UserRecord",
          body: $body,
          footer: $footer,
          beforeClose: function(cb) {
            if (apply) {
              var row = self.getRow({
                label: $body.find("#name").val(),
                value: $body.find("#value").val(),
                type: $body.find("#type").val(),
                title: $body.find("#type").val(),
                context: context
              });
              self.editEntry(row, context, false, function() {
                table.append(row);
              });
            }
            cb();
          },
          onRender: function() {
            dialog = this;
          }
        });
      },
      addPath: function(table, context) {
        var self = this;

        this.instance.openFileSelector(null, false, true, function(path) {
          if (!path)
            return;
          self.server.POST(["analysis", self.instance.state.get("ID"), context, path], function(res) {
            var row = self.getRow({
              label: "",
              value: path,
              type: "string",
              title: "",
              context: context
            });
            table.append(row);
          });
        });
      },

      deleteEntry: function(thisRow, context) {
        var self = this;
        var cb = function() {
          thisRow.remove();
        };
        cb();

        var deletedLabel = thisRow.attr("data-label");
        var args = ["analysis", self.instance.state.get("ID"), "module", self.selectedName, context, deletedLabel];
        self.server.DELETE(args, function(res) {
          cb();
        });
      },

      editEntry: function(thisRow, context, ask, cb) {
        var self = this;
        ask = ask === undefined ? true : false;
        var clickedLabel = thisRow.attr("data-label");
        var clickedType = thisRow.attr("data-type");
        var clickedValue = cast_to_pxltype(thisRow.attr("data-content"), clickedType);
        if (ask) {
          var transmit = function(input) {
            var args = ["analysis", self.instance.state.get("ID"), "module", self.selectedName, context, clickedLabel];
            self.server.POST(args, function(res) {
              thisRow.attr("data-content", String(input)).find("#value").html(String(input));
              if ($.isFunction(cb)) cb();
            }, {
              value: input
            });
          };
          //file selector for file options
          if (clickedLabel == "filename" || clickedLabel == "File names") {
            var multiMode = (clickedLabel == "File names");
            self.instance.openFileSelector(null, multiMode, false, function(paths) {
              if (!paths)
                return;
              if (multiMode) {
                var pathString = '["';
                pathString = pathString + paths.join('","');
                pathString = pathString + '"]';
                paths = pathString;
              }
              transmit(paths);
            });
            //selection list for string vectors
          } else if (clickedType == "string_vector") {
            self.handleStringVector(thisRow, context);
            //regular prompt for all the others
          } else {
            self.instance.prompt("Enter a new value for " + clickedLabel + " of type " + clickedType, function(input) {
              transmit(input);
            }, {
              defaultValue: clickedValue
            });
          }
        } else {
          var args = ["analysis", self.instance.state.get("ID"), "module", self.selectedName, context, clickedLabel];
          self.server.POST(args, function() {
            if ($.isFunction(cb)) cb();
          }, {
            value: clickedValue
          });
        }
      },

      handleStringVector: function(thisRow, context) {
        var self = this;
        var clickedLabel = thisRow.attr("data-label");
        var vecString = thisRow.attr("data-content");

        stringVectorWindow.init(self, vecString);

        this.instance.dialog({
          header: "<i class='glyphicon glyphicon-info-sign'></i>" +
            clickedLabel,
          body: stringVectorWindow.getNode(),
          beforeClose: function(cb) {
            var newEntryVec = stringVectorWindow.getVector()
            var args = ["analysis", self.instance.state.get("ID"), "module", self.selectedName, context, clickedLabel];
            self.server.POST(args, function(res) {
              if (stringVectorWindow.selected === null)
                rowContent = newEntryVec.join(",");
              else
                rowContent = stringVectorWindow.selected;
              thisRow.attr("data-content", String(newEntryVec.join(","))).find("#value").html(rowContent);
              if ($.isFunction(cb)) cb();
            }, {
              value: [newEntryVec]
            });
          }
        });

      }
    });



    return PropertyView;

  });
