define(["jquery", "emitter", "./plumbObjects"],
  function($, Emitter, plumbObjects) {

    var ModuleView = Emitter._extend({

      init: function(instance, manager) {
        var self = this;

        this.instance = instance;
        this.manager = manager;
        this.server = instance.Server;
        
        this.viewNode = null;
        this.availableModules = {};
      },

      setup: function($node) {
        var self = this;
        this.viewNode = $node;

        this.setAvailableNodes();
      },

      setAvailableNodes: function() {
        var self = this;
        this.server.GET(["analysis", this.instance.state.get("ID"), "availablemodules"], function(res){
          for (var i = 0; i<res.length; i = i+1){
            self.availableModules[res[i]] = {};
            var tempModuleNode = new plumbObjects.moduleNode(self.instance, self, res[i]);
            self.viewNode.append(tempModuleNode.jqueryNodeSmall());
          }
        });
      },

      reload: function() {
        this.reset();
        this.setAvailableNodes();
      },

      reset: function() {
        this.availableModules = {};
        this.viewNode.empty();
      }
    });


    return ModuleView;

  });