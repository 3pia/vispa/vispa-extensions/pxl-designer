# -*- coding: utf-8 -*-

"""
PXL analysis designer extension.
"""

import cherrypy
from vispa.server import AbstractExtension
from vispa.controller import AbstractController
from vispa import AjaxException
from vispa.rest import RESTDispatcher, RESTController, GET, POST, DELETE
from uuid import uuid4
import json as JSON
import os
import time


class PXLDesignerExtension(AbstractExtension):

    def __init__(self, *args, **kwargs):
        super(PXLDesignerExtension, self).__init__(*args, **kwargs)

        self.last_id = -1
        self.id_map  = {}

    def name(self):
        return "pxldesigner"

    def dependencies(self):
        return []

    def config(self):
        return {
            "/"      : { "request.dispatch": RESTDispatcher() },
            "/static": { "request.dispatch": cherrypy.dispatch.Dispatcher() }
        }

    def setup(self):
        self.add_controller(PXLDesignerController(self))
        self.add_workspace_directoy()

    def new_analysis(self, xmlpath=None):
        key = uuid4().hex

        self.last_id += 1
        id = self.last_id

        analysis = self.get_workspace_instance("PXLAnalysis", key=key, id=id, xmlpath=xmlpath)

        self.id_map[id] = key

        return analysis

    def get_analysis(self, id):
        key = self.id_map.get(id, None)
        if key is None:
            raise AjaxException("analysis with id '%s' not found" % id)
        return self.get_workspace_instance("PXLAnalysis", key=key)


class PXLDesignerController(AbstractController, RESTController):

    def __init__(self, extension):
        AbstractController.__init__(self)
        RESTController.__init__(self)

        self.extension = extension

    #rpc for the job execution (requires a stream)
    def getrpc(self):
        windowId = cherrypy.request.private_params.get("_windowId", None)
        viewId = cherrypy.request.private_params.get("_viewId", None)
        return self.get("proxy", "PXLDesignerRpc", self.get("combined_id"),
                        window_id=windowId, view_id=viewId)

    def analysis(self, id):
        return self.extension.get_analysis(int(id))

    @GET("analysis")
    def new_analysis(self):
        """
        Creates a new analysis. Returns its id.
        """
        return self.extension.new_analysis().id

    @cherrypy.tools.ajax(encoded=True)
    @GET("analysis/:id(^\d+$)")
    def get_analysis_by_id(self, id):
        """
        Gets an analysis by its id. Returns its meta data.
        """
        return self.analysis(id).get_meta_data(json=True)

    @GET("analysis/:xmlpath")
    def open_analysis_from_xml(self, xmlpath):
        """
        Opens an analysis from an xml file. Returns its meta data.
        """
        return self.extension.new_analysis(xmlpath=xmlpath).id

    @POST("analysis/:id(^\d+$)/save/:xmlpath")
    def save_analysis(self, id, xmlpath):
        """
        Saves an analysis.
        """
        self.analysis(id).save(xmlpath)

    @POST("analysis/:id(^\d+$)/reload")
    def reload_analysis(self, id):
        """
        Reloads an analysis.
        """
        self.analysis(id).reload()

    @POST("analysis/:id(^\d+$)/execution/:command/:xmlpath")
    def execute_analysis(self, id, command, xmlpath):
        self.release_session()
        rpc = self.getrpc()
        rpc.start(command, xmlpath)
        self.release_database()
        """
        Executes an analysis.
        """

    @GET("analysis/:id(^\d+$)/execution")
    def check_analysis_execution(self, id):
        self.release_session()
        rpc = self.getrpc()
        self.release_database()
        return {"running": rpc.check_analysis_execution()}
        """
        Checks the status of the analysis execution.
        """

    @DELETE("analysis/:id(^\d+$)/execution")
    def abort_analysis_execution(self, id):
        self.release_session()
        rpc = self.getrpc()
        rpc.abort()
        self.release_database()
        """
        Aborts an analysis execution.
        """

    @cherrypy.tools.ajax(encoded=True)
    @GET("analysis/:id(^\d+$)/env")
    def get_analysis_environment(self, id):
        """
        Gets the environment data of an analysis.
        """
        return self.analysis(id).get_env(json=True)

    @POST("analysis/:id(^\d+$)/plugin_paths/:path")
    def add_plugin_path(self, id, path):
        """
        Adds a path to the plugin path of the analysis.
        """
        self.analysis(id).add_plugin_path(path)

    @POST("analysis/:id(^\d+$)/search_paths/:path")
    def add_search_path(self, id, path):
        """
        Adds a search path to the analysis.
        """
        self.analysis(id).add_search_path(path)

    @POST("analysis/:id(^\d+$)/python_path/:path")
    def add_python_path(self, id, path):
        """
        Adds a path to the python path of the analysis.
        """
        self.analysis(id).add_python_path(path)

    @GET("analysis/:id(^\d+$)/pwd")
    def get_analysis_directory(self, id):
        """
        Gets the working directory of an analysis.
        """
        return self.analysis(id).pwd

    @POST("analysis/:id(^\d+$)/pwd/:pwd")
    def change_analysis_directory(self, id, pwd):
        """
        Changes the working directory of an analysis.
        """
        self.analysis(id).pwd = pwd

    @cherrypy.tools.ajax(encoded=True)
    @GET("analysis/:id(^\d+$)/availablemodules")
    def get_available_modules(self, id):
        """
        Gets all available modules of an analysis. This depends on the current analysis environment.
        """
        return self.analysis(id).get_available_modules(json=True)

    @cherrypy.tools.ajax(encoded=True)
    @GET("analysis/:id(^\d+$)/module_names")
    def get_module_names(self, id):
        """
        Gets the names of all modules of an analysis.
        """
        return self.analysis(id).get_module_names(json=True)

    #gets the required information for the analysis view 
    @cherrypy.tools.ajax(encoded=True)
    @GET("analysis/:id(^\d+$)/modules")
    def get_modules(self, id):
        """
        Gets the names of all modules of an analysis.
        """
        return self.analysis(id).get_modules(json=True)

    @cherrypy.tools.ajax(encoded=True)
    @GET("analysis/:id(^\d+$)/module/:name")
    def get_module(self, id, name, selection="all"):
        """
        Gets a specific module of an analysis.
        """
        return self.analysis(id).get_module(name, selection, json=True)

    @cherrypy.tools.ajax(encoded=True)
    @GET("analysis/:id(^\d+$)/module/:sourcename/:targetname/:type")
    def copy_module(self, id, sourcename, targetname, type):
        """
        Copies a specific module of the analysis and attach its copy to it.
        """
        return self.analysis(id).copy_module(sourcename, targetname, type)

    @cherrypy.tools.ajax(encoded=True)
    @GET("analysis/:id(^\d+$)/module/:name/:type")
    def add_module(self, id, name, type):
        """
        Adds a new module to an analysis.
        """
        return self.analysis(id).add_module(name, type, json=True)

    @POST("analysis/:id(^\d+$)/module/:name/skeleton/:target_dir/:target_name")
    def make_skeleton(self, id, name, target_dir, target_name):
        #todo: remove hard code of skeleton !!!
        skeleton_path = os.path.join(os.path.dirname(__file__), "skeleton.py")
        with open(skeleton_path, "r") as skeleton:
            target_path = target_dir+"/"+target_name
            content = skeleton.read()
            user = cherrypy.request.workspace.login
            workspace = cherrypy.request.workspace.host
            date = time.strftime("%d.%m.%y")+", "+time.strftime("%H:%M:%S")
            content = content.replace("%USER", user)
            content = content.replace("%WORKSPACE", workspace)
            content = content.replace("%DATE", date)
            fs = self.get("fs")
            fs.save_file(target_path, content, utf8=True)

        self.analysis(id).set_module_option(name, "filename", target_path, json=False)

    @cherrypy.tools.ajax(encoded=True)
    @POST("analysis/:id(^\d+$)/module/:name/:enable")
    def enable_module(self, id, name, enable):
        """
        Enable/Disable a specified module.
        """
        return self.analysis(id).enable_module(name, enable)

    @DELETE("analysis/:id(^\d+$)/module/:name")
    def remove_module(self, id, name):
        """
        Removes a specific module from an analysis.
        """
        self.analysis(id).remove_module(name)

    @cherrypy.tools.ajax(encoded=True)
    @GET("analysis/:id(^\d+$)/connections")
    def get_connections(self, id):
        """
        Gets all connections of an analysis.
        """
        return self.analysis(id).get_connections(json=True)

    @GET("analysis/:id(^\d+$)/connection/:sourcemodule/:source/:sinkmodule/:sink")
    def check_connection(self, id, sourcemodule, source, sinkmodule, sink):
        """
        Checks if two modules are connected.
        """
        return self.analysis(id).check_connection(sourcemodule, source, sinkmodule, sink)

    @POST("analysis/:id(^\d+$)/connection/:sourcemodule/:source/:sinkmodule/:sink")
    def add_connection(self, id, sourcemodule, source, sinkmodule, sink):
        """
        Creates a new connection between two modules.
        """
        return self.analysis(id).add_connection(sourcemodule, source, sinkmodule, sink)

    @DELETE("analysis/:id(^\d+$)/connection/:sourcemodule/:source/:sinkmodule/:sink")
    def remove_connection(self, id, sourcemodule, source, sinkmodule, sink):
        """
        Removes a connection between two modules.
        """
        return self.analysis(id).remove_connection(sourcemodule, source, sinkmodule, sink)

    @cherrypy.tools.ajax(encoded=True)
    @GET("analysis/:id(^\d+$)/module/:name/options/:key")
    def get_module_option(self, id, name, key):
        """
        Gets a specific option of a module.
        """
        return self.analysis(id).get_module_option(name, key, json=True)

    @cherrypy.tools.ajax(encoded=True)
    @GET("analysis/:id(^\d+$)/modules/options")
    def get_all_module_options(self, id):
        """
        Gets a dictionary of all modules and their options.
        """
        return self.analysis(id).get_all_module_options(json=True)

    @cherrypy.tools.json_in()
    @POST("analysis/:id(^\d+$)/module/:name/options/:key")
    def set_module_option(self, id, name, key):
        """
        Sets the value of a specific option of a module.
        """
        values = cherrypy.request.json["value"]

        if not isinstance(values, list):
            values = [values]

        for value in values:
            json = False
            if isinstance(value, (dict, list)):
                value = JSON.dumps(value)
                json  = True

            self.analysis(id).set_module_option(name, key, value, json=json)

    @POST("analysis/:id(^\d+$)/module/rename/:name/:new_name")
    def set_module_name(self, id, name, new_name):
        self.analysis(id).set_module_name(name, new_name);

    @cherrypy.tools.ajax(encoded=True)
    @GET("analysis/:id(^\d+$)/module/:name/userrecords/:key")
    def get_module_userrecord(self, id, name, key):
        """
        Get a specific user record of a module.
        """
        return self.analysis(id).get_module_userrecord(name, key, json=True)

    @cherrypy.tools.ajax(encoded=True)
    @DELETE("analysis/:id(^\d+$)/module/:name/userrecords/:key")
    def get_module_userrecord(self, id, name, key):
        """
        Delete a specific user record of a module.
        """
        return self.analysis(id).delete_module_userrecord(name, key)    

    @cherrypy.tools.json_in()
    @POST("analysis/:id(^\d+$)/module/:name/userrecords/:key")
    def set_module_userrecord(self, id, name, key):
        """
        Sets the value of a specific user record of a module.
        """
        values = cherrypy.request.json["value"]

        if not isinstance(values, list):
            values = [values]

        for value in values:
            json = False
            if isinstance(value, (dict, list)):
                value = JSON.dumps(value)
                json  = True

            self.analysis(id).set_module_userrecord(name, key, value, json=json)

    @cherrypy.tools.ajax(encoded=True)    
    @GET("analysis/:id(^\d+$)/pxl/usages")
    def get_pxl_usages(self, id):
        return self.analysis(id).get_pxl_usages(json=True)

    @cherrypy.tools.ajax(encoded=True)    
    @GET("analysis/:id(^\d+$)/pxl/types")
    def get_pxl_types(self, id):
        return self.analysis(id).get_pxl_types(json=True)
