# -*- coding: utf-8 -*-

"""
PXL related constants and helpers.
"""

__all__ = ["USAGE_NAMES", "TYPE_NAMES", "get_pxltype", "cast_to_pxl", "empty_pxl_value"]


from pxl import modules
import json as JSON


USAGE_NAMES = {}
TYPE_NAMES  = {}


# add usage and type flags to the locals
# and fill the *_NAMES mappings
context = locals()
optdesc = modules.OptionDescription
for key in dir(optdesc):
    member = getattr(optdesc, key)

    if key.startswith("USAGE_"):
        context[key]        = member
        USAGE_NAMES[member] = key[6:].lower()
        __all__.append(key)

    elif key.startswith("TYPE_"):
        context[key]       = member
        TYPE_NAMES[member] = key[5:].lower()
        __all__.append(key)


# gets the pxl type of a pythonic value
def get_pxltype(value):
    if isinstance(value, int):
        return TYPE_LONG
    elif isinstance(value, float):
        return TYPE_DOUBLE
    elif isinstance(value, (str, unicode)):
        return TYPE_STRING
    elif isinstance(value, bool):
        return TYPE_BOOLEAN
    elif isinstance(value, list) and all([isinstance(elem, (str, unicode)) for elem in value]):
        return TYPE_STRING_VECTOR

    raise TypeError("cannot determine pxl type: %s" % value)


# casts a pythonic (or even json encoded) value to a valid pxl value
def cast_to_pxl(value, pxltype):
    try:
        if value is None:
            return empty_pxl_value(pxltype)
        elif pxltype == TYPE_LONG:
            return int(value)
        elif pxltype == TYPE_DOUBLE:
            return float(value)
        elif pxltype == TYPE_BOOLEAN:
            if isinstance(value, bool):
                return value
            elif isinstance(value, (str, unicode)) and value.lower() in ["true", "false"]:
                return value.lower() == "true"
            raise ValueError("cannot cast '%s' to bool" % value)
        elif pxltype == TYPE_STRING:
            return str(value)
        elif pxltype == TYPE_STRING_VECTOR:
            if isinstance(value, (list, tuple, set)):
                return [str(elem) for elem in value]
            elif isinstance(value, (str, unicode)):
                try:
                    return [str(elem) for elem in JSON.loads(value)]
                except: pass
            raise ValueError("cannot cast '%s' to string vector" % value)
        raise ValueError("pxl type '%s' unknown" % pxltype)
    except Exception as e:
        raise Exception("cannot cast to pxl type: %s" % str(e))


# get an empty value for a specific pxl type
def empty_pxl_value(pxltype):
    if pxltype == TYPE_STRING:
        return ""
    elif pxltype == TYPE_STRING_VECTOR:
        return [""]
    elif pxltype == TYPE_DOUBLE:
        return .0
    elif pxltype == TYPE_LONG:
        return 0
    elif pxltype == TYPE_BOOLEAN:
        return True

    raise ValueError("pxl type '%s' unknown" % pxltype)
