# -*- coding: utf-8 -*-

"""
Workspace code that wraps a pxl analysis instance.
"""

import os, time
import sys
import tempfile
import json as JSON
from collections import OrderedDict
from pxl import modules, core, xml
import logging
from vispa.remote import AjaxException, raise_ajax
import pxlutils

#rpc imports
import vispa
import time
import threading
import select
import pty
import signal
from subprocess import Popen, PIPE


from pwd import getpwuid

logger = logging.getLogger(__name__)


def expand(path):
    return os.path.expanduser(os.path.expandvars(path))


class PXLAnalysis(object):

    def __init__(self, id, xmlpath=None):
        super(PXLAnalysis, self).__init__()

        self.id      = id
        self.xmlpath = None

        self.analysis       = modules.Analysis()
        self.module_factory = modules.ModuleFactory.instance()
        self.plugin_manager = core.PluginManager_instance()

        self.search_paths = []
        self.plugin_paths = []

        self.plugin_manager.loadPlugins()

        if xmlpath is not None:
            self.load(xmlpath)

    @raise_ajax
    def get_meta_data(self, json=False):
        data = {
            "meta": [
                {
                    "key": "Id",
                    "value": self.id,
                    "description": "ID of this analysis."
                },
                {
                    "key": "Xmlpath",
                    "value": self.xmlpath,
                    "description": "Absolute path of this analysis file."
                },
                {
                    "key": "Pwd",
                    "value": self.analysis.getOutputPath(),
                    "description": "The analysis' output path."
                },
                {
                    "key": "Owner",
                    "value": getpwuid(os.stat(self.xmlpath).st_uid).pw_name,
                    "description": "Owner of the analysis file."
                },
                {
                    "key": "Modified",
                    "value": time.ctime(os.path.getmtime(self.xmlpath)),
                    "description": "Last modification time."
                }
            ]
        }
        return data if not json else JSON.dumps(data)


    @raise_ajax
    def load(self, xmlpath, setpath=True):
        xmlpath = expand(xmlpath)

        importer = xml.AnalysisXmlImport()
        importer.open(xmlpath)
        importer.parseInto(self.analysis)
        importer.close()

        # finally, set the xml path
        if setpath:
            self.xmlpath = xmlpath

    @raise_ajax
    def save(self, xmlpath, setpath=True):
        xmlpath = expand(xmlpath)

        # simple xml export
        exporter = xml.AnalysisXmlExport()
        exporter.open(xmlpath)
        exporter.writeObject(self.analysis)
        exporter.close()

        # finally, set the xml path
        if setpath:
            self.xmlpath = xmlpath

    @raise_ajax
    def reload(self):
        # save into tempfile, clear analysis object, load again, delete tempfile
        tmp = tempfile.mkstemp(".xml")[1]

        self.save(tmp, setpath=False)

        self.analysis.shutdown()

        self.load(tmp, setpath=False)

        os.remove(tmp)

    @property
    @raise_ajax
    def pwd(self):
        return self.analysis.getOutputPath()

    @pwd.setter
    @raise_ajax
    def pwd(self, path):
        self.analysis.setOutputPath(expand(path))

    @raise_ajax
    def add_plugin_path(self, path):
        path = expand(path)
        self.plugin_manager.loadPluginsFromDirectory(path)
        self.plugin_paths.append(path)

    @raise_ajax
    def add_search_path(self, path):
        path = expand(path)
        self.analysis.addSearchPath(path)
        self.search_paths.append(path)

    @raise_ajax
    def add_python_path(self, path):
        path = expand(path)
        sys.path.append(path)

    @raise_ajax
    def get_env(self, json=False):
        env = {
            "plugin_paths": self.plugin_paths,
            "search_paths": self.search_paths,
            "python_path" : sys.path
        }
        return env if not json else JSON.dumps(env)

    @raise_ajax
    def get_available_modules(self, json=False):
        types = self.module_factory.getAvailableModules()
        #remove possible duplicates (possible pxl bug!)
        types = OrderedDict.fromkeys(types).keys()
        
        #sort the modules but the defaults
        default_types = ["File Input", "File Output", "PyAnalyse", "PyDecide", "PyGenerator", "PyModule", "PySwitch"]
        sort_types, no_sort_types = [], []
        for type in types:
            if type in default_types: 
                no_sort_types.append(type)
            else:
                sort_types.append(type)
        types = no_sort_types + sorted(sort_types)

        return types if not json else JSON.dumps(types)

    @raise_ajax
    def get_module_names(self, json=False):
        modules = self.analysis.getModules()
        names   = [m.getName() for m in modules]
        return names if not json else JSON.dumps(names)

    @raise_ajax
    def get_modules(self, json=False):
        names   = [m.getName() for m in self.analysis.getModules()]
        modules = []
        for name in names:
            module = self.analysis.getModule(name)

            mtype = module.getType()
            userrecords = (ModuleAccessor.parse(module, "userrecords"))["userrecords"]
            sinks   = (ModuleAccessor.parse(module, "sinks"))["sinks"]
            sources = (ModuleAccessor.parse(module, "sources"))["sources"]
            positions = {}
            for i in range(0, len(userrecords)):
                UR = userrecords[i]
                if UR["key"] == "xPos":
                    positions["x"] = UR["value"]
                if UR["key"] == "yPos":
                    positions["y"] = UR["value"]
            
            trunk = {
                "name": name,
                "sinks": sinks,
                "sources": sources,
                "positions": positions,
                "type": mtype,
                "enabled": module.isEnabled()
                }

            modules.append(trunk)
        return modules if not json else JSON.dumps(modules)

    @raise_ajax
    def set_module_name(self, name, new_name):
        self.analysis.getModule(name).setName(new_name)
        for connection in self.analysis.getConnections():
            if connection.sourceModuleName == name:
                connection.sourceModuleName = new_name
            if connection.sinkModuleName == name:
                connection.sinkModuleName = new_name

    @raise_ajax
    def add_module(self, name, type, json=False):
        self.analysis.addModule(type, name, "0")
        module = self.analysis.getModule(name)
        trunk = {
            "sinks": (ModuleAccessor.parse(module, "sinks"))["sinks"],
            "sources": (ModuleAccessor.parse(module, "sources"))["sources"],
            "enabled": module.isEnabled()
        }
        return trunk if not json else JSON.dumps(trunk)

    @raise_ajax
    def copy_module(self, sourcename, targetname, type, json=True):
        self.add_module(targetname, type)
        sourcedata = self.get_module(sourcename, selection="all")
        for entry in sourcedata["userrecords"]:
            self.set_module_userrecord(targetname, entry["key"], entry["value"])
        for entry in sourcedata["options"]:
            self.set_module_option(targetname, entry["key"], entry["value"])
        targetdata = self.get_module(targetname, selection="all", json=False)

        return targetdata if not json else JSON.dumps(targetdata)

    @raise_ajax
    def remove_module(self, name):
        self.analysis.removeModule(name)

    @raise_ajax
    def get_module(self, name, selection, json=False):
        module = self.analysis.getModule(name)
        module.reload()
        data   = ModuleAccessor.parse(module, selection)
        data["enabled"] = module.isEnabled()

        return data if not json else JSON.dumps(data)

    @raise_ajax
    def enable_module(self, name, enable):
        module = self.analysis.getModule(name)
        module.setEnabled(JSON.loads(enable))
        

    @raise_ajax
    def get_connections(self, json=False):
        connections = []
        for conn in self.analysis.getConnections():
            connections.append(OrderedDict([
                ("source_module", conn.sourceModuleName),
                ("source"       , conn.sourceName),
                ("sink_module"  , conn.sinkModuleName),
                ("sink"         , conn.sinkName)
            ]))
        return connections if not json else JSON.dumps(connections)

    @raise_ajax
    def check_connection(self, sourcemodule, source, sinkmodule, sink):
        target = (sourcemodule, source, sinkmodule, sink)
        for data in self.get_connections():
            if tuple(data.values()) == target:
                return True
        return False

    @raise_ajax
    def add_connection(self, sourcemodule, source, sinkmodule, sink):
        self.analysis.connectModules(sourcemodule, source, sinkmodule, sink)

    @raise_ajax
    def remove_connection(self, sourcemodule, source, sinkmodule, sink):
        self.analysis.disconnectModules(sourcemodule, source, sinkmodule, sink)

    @raise_ajax
    def get_module_option(self, name, key, json=False):
        module = self.analysis.getModule(name)
        data   = ModuleAccessor.get_option(module, key)
        return data if not json else JSON.dumps(data)

    @raise_ajax
    def get_all_module_options(self, json=False):
        options = {}
        for _module in self.get_modules(json=False):
            module = self.analysis.getModule(_module["name"])
            options[_module["name"]]   = ModuleAccessor.get_options(module)

        return options if not json else JSON.dumps(options)

    @raise_ajax
    def set_module_option(self, name, key, value, json=False):
        if json:
            try:
                value = JSON.loads(value)
            except:
                raise Exception("invalid json format")

        module = self.analysis.getModule(name)

        # cast to correct pxl type
        pxltype = module.getOptionDescription(key).type
        value   = pxlutils.cast_to_pxl(value, pxltype)

        module.setOption(key, value)

    @raise_ajax
    def get_module_userrecord(self, name, key, json=False):
        module = self.analysis.getModule(name)
        data   = ModuleAccessor.get_userrecord(module, key)
        return data if not json else JSON.dumps(data)

    @raise_ajax
    def delete_module_userrecord(self, name, key):
        module = self.analysis.getModule(name)
        module.eraseUserRecord(key)

    @raise_ajax
    def set_module_userrecord(self, name, key, value, json=False):
        if json:
            try:
                value = JSON.loads(value)
            except:
                raise Exception("invalid json format")

        module = self.analysis.getModule(name)

        # safety cast
        pxltype = pxlutils.get_pxltype(value)
        value   = pxlutils.cast_to_pxl(value, pxltype)

        module.setUserRecord(key, core.Native_toVariant(value))

    @raise_ajax
    def get_pxl_usages(self, json=False):
        return pxlutils.USAGE_NAMES if not json else JSON.dumps(pxlutils.USAGE_NAMES)

    @raise_ajax
    def get_pxl_types(self, json=False):
        return pxlutils.TYPE_NAMES if not json else JSON.dumps(pxlutils.TYPE_NAMES)


class ModuleAccessor(object):

    selections = {
        "maindata"   : ["maindata"],
        "options"    : ["options"],
        "userrecords": ["userrecords"],
        "sources"    : ["sources"],
        "sinks"      : ["sinks"],
        "all"        : ["maindata", "options", "userrecords", "sinks", "sources"],
        "properties" : ["maindata", "options", "userrecords"],
        "ports"      : ["sources", "sinks"]
    }

    @classmethod
    def parse(cls, module, selection):
        if selection not in cls.selections:
            raise Exception("invalid selection: %s" % selection)

        data = {}
        for key in cls.selections[selection]:
            data[key] = getattr(cls, "get_" + key)(module)

        return data

    @classmethod
    def get_maindata(cls, module):
        data = []
        
        # name
        name    = module.getName()
        pxltype = pxlutils.get_pxltype(name)
        data.append({
            "key"        : "name",
            "value"      : module.getName(),
            "label"      : "Name",
            "description": "The unique name of the module.",
            "type"       : pxltype,
            "type_name"  : pxlutils.TYPE_NAMES[pxltype]
        })

        # type
        mtype   = module.getType()
        pxltype = pxlutils.get_pxltype(mtype)
        data.append({
            "key"        : "type",
            "value"      : mtype,
            "label"      : "Type",
            "description": "The type of the module.",
            "type"       : pxltype,
            "type_name"  : pxlutils.TYPE_NAMES[pxltype]
        })

        # version
        version = module.getVersion()
        pxltype = pxlutils.get_pxltype(version)
        data.append({
            "key"        : "version",
            "value"      : version,
            "label"      : "Version",
            "description": "The version of the module.",
            "type"       : pxltype,
            "type_name"  : pxlutils.TYPE_NAMES[pxltype]
        })

        # enabled
        enabled = module.isEnabled()
        pxltype = pxlutils.get_pxltype(enabled)
        data.append({
            "key"        : "enabled",
            "value"      : enabled,
            "label"      : "Enabled",
            "description": "Is the module enabled?",
            "type"       : pxltype,
            "type_name"  : pxlutils.TYPE_NAMES[pxltype]
        })

        # compilation date
        ur      = "Compilation Date"
        ur      = module.getUserRecord(ur) if module.hasUserRecord(ur) else ""
        pxltype = pxlutils.get_pxltype(ur)
        data.append({
            "key"        : "cdata",
            "value"      : ur,
            "label"      : "Compilation date",
            "description": "The compilation date of the module.",
            "type"       : pxltype,
            "type_name"  : pxlutils.TYPE_NAMES[pxltype]
        })

        # compilation time
        ur      = "Compilation Time"
        ur      = module.getUserRecord(ur) if module.hasUserRecord(ur) else ""
        pxltype = pxlutils.get_pxltype(ur)
        data.append({
            "key"        : "ctime",
            "value"      : ur,
            "label"      : "Compilation time",
            "description": "The compilation time of the module.",
            "type"       : pxltype,
            "type_name"  : pxlutils.TYPE_NAMES[pxltype]
        })

        return data

    @classmethod
    def get_option(cls, module, key):
        option = module.getOptionDescription(key)
        return {
            "key"        : key,
            "value"      : module.getOption(key),
            "type"       : option.type,
            "type_name"  : pxlutils.TYPE_NAMES.get(option.type, None),
            "usage"      : option.usage,
            "usage_name" : pxlutils.USAGE_NAMES.get(option.usage, None),
            "description": option.description
        }

    @classmethod
    def get_options(cls, module):
        options = []
        for option in module.getOptionDescriptions():
            options.append(cls.get_option(module, option.name))
        return options

    @classmethod
    def get_userrecord(cls, module, key):
        value        = module.getUserRecord(key)
        pxltype      = pxlutils.get_pxltype(value)
        pxltype_name = pxlutils.TYPE_NAMES.get(pxltype, None)
        return {
            "key"      : key,
            "value"    : value,
            "type"     : pxltype,
            "type_name": pxltype_name
        }

    @classmethod
    def get_userrecords(cls, module):
        records = []
        for key in module.getUserRecords().getContainer():
            records.append(cls.get_userrecord(module, key))
        return records

    @classmethod
    def get_sources(cls, module):
        sources = []
        for source in module.getSourceDescriptions():
            sources.append({
                "name"       : source.name,
                "description": source.description
            })
        return sources

    @classmethod
    def get_sinks(cls, module):
        sinks = []
        for sink in module.getSinkDescriptions():
            sinks.append({
                "name"       : sink.name,
                "description": sink.description
            })
        return sinks




class PXLDesignerRpc:
    MAX_BURST    = 1000 # max bytes transmitted at once
    MAX_RATE     = 2000 # desired baud rate for transmission
    BURST_DELAY  = 0.25 # delay between transmission when burst buffer is exausted
    BURST_BUFFER = 8000 # burst buffer size
    SIGTEM_SIGKILL_DELAY = 0.1

    def __init__(self, window_id, view_id):

        self._view_id = view_id
        self._window_id = window_id
        self._topic = "extension.%s.socket" % self._view_id

        self._thread = None
        self._abort = False

        self._pty_master, self._pty_slave = pty.openpty()
        self._pty_fd = os.fdopen(self._pty_master, 'r')
        self._pty_fno = self._pty_fd.fileno()
        self._popen = None

        logger.debug("PXLDesignerRpc created")

    #so far equivalent to abort (might be extended in the future)
    def close(self):    
        self._abort = True

    def _send(self, topic, data=None):
        vispa.remote.send_topic(
            self._topic + "." + topic, window_id=self._window_id, data=data)

    def check_analysis_execution(self):
        return bool(self._thread and self._thread.is_alive())

    def start(self, cmd, base):
        if self.check_analysis_execution():
            return "There is already a job running"

        self._cmd = expand(cmd)
        self._base = expand(base)
        self._starttime = time.time()

        try:
            self._popen = Popen(self._cmd, cwd=self._base, shell=True,
                                stdin=PIPE, stdout=self._pty_slave, stderr=self._pty_slave,
                                close_fds=True, preexec_fn=os.setsid)
        except Exception as e:
            return str(e)

        # send result via socket to ensure in order processing
        self._send('start', {
            "command": self._cmd,
            "base": self._base,
        })

        logger.debug("PXLDesignerRpc starting _stream")

        self._thread = threading.Thread(target=self._stream)
        self._thread.daemon = True
        self._thread.start()

        return ""

    def _stream(self):
        returncode = None
        self._abort = False
        read_time = time.time()
        read_amount = 0
        while not self._abort:
            r, _, _ = select.select([self._pty_fno], [], [], 0.1)
            if read_amount >= PXLDesignerRpc.BURST_BUFFER:
                time.sleep(PXLDesignerRpc.BURST_DELAY)
            elif self._pty_fno in r:
                data = os.read(self._pty_fno, PXLDesignerRpc.MAX_BURST)
                read_amount += len(data)
                self._send('data', data)
            if read_amount > 0:
                read_amount = int(max(0, read_amount - PXLDesignerRpc.MAX_RATE*(time.time()-read_time)))
                read_time = time.time()

            returncode = self._popen.poll()
            if returncode is not None:
                break

        # soft kill
        if self._abort:
            pgid = os.getpgid(self._popen.pid)
            os.killpg(pgid, signal.SIGTERM)
            returncode = -signal.SIGTERM
            time.sleep(PXLDesignerRpc.SIGTEM_SIGKILL_DELAY)
            try:
                os.killpg(pgid, signal.SIGKILL)
            except OSError as e:
                if e.errno != 3:  # No such process
                    raise e
            else:
                returncode = -signal.SIGKILL

        # check remaing data
        read_burst = min(   int(PXLDesignerRpc.MAX_RATE*PXLDesignerRpc.BURST_DELAY),
                            PXLDesignerRpc.MAX_BURST)
        read_delay = 0
        r = True
        while r:
            r, _, _ = select.select([self._pty_fno], [], [], 0)
            if self._pty_fno in r:
                if read_delay:
                    time.sleep(read_delay)
                else:
                    read_delay = float(read_burst)/PXLDesignerRpc.MAX_RATE
                self._send('data', os.read(self._pty_fno, read_burst))

        runtime = round(time.time() - self._starttime, 2)

        self._send('done', {"success": True, "runtime": runtime,
                            "returncode": returncode, "aborted": self._abort})

        logger.debug("PXLDesignerExecuteRpc _stream finished")

    def abort(self):
        if not self.check_analysis_execution():
            return False
        self._abort = True
        return True
